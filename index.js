
const FIRST_NAME = "Cristian-Alin";
const LAST_NAME = "Puican";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function numberParser(value) {

    if(value>=Number.MIN_SAFE_INTEGER && value<=Number.MAX_SAFE_INTEGER){
        return Math.floor(parseInt(value));
    }
    else{
        return NaN;
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

